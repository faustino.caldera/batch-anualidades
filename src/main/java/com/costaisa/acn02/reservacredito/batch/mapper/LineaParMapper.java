package com.costaisa.acn02.reservacredito.batch.mapper;

import com.costaisa.acn02.reservacredito.batch.entity.SalidaPar;
import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;
import com.costaisa.acn02.reservacredito.batch.model.LineaImpar;
import com.costaisa.acn02.reservacredito.batch.model.LineaPar;

public class LineaParMapper {
	public static LineaPar map(final TratamientoLinea tratamientoLinea) {
		return new LineaPar(tratamientoLinea.getNombre(), tratamientoLinea.getCantidad());
	}
	public static SalidaPar map(final LineaPar lineaImpar) {
		return new SalidaPar(null, lineaImpar.getNombre(), lineaImpar.getCantidad());
	}
}
