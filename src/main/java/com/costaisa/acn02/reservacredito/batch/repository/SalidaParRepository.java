package com.costaisa.acn02.reservacredito.batch.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.costaisa.acn02.reservacredito.batch.entity.SalidaPar;

@Repository
public interface SalidaParRepository extends PagingAndSortingRepository<SalidaPar, Long>{

}
