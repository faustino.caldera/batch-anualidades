package com.costaisa.acn02.reservacredito.batch.job;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.catalina.startup.ClassLoaderFactory.Repository;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ListLineaReader implements ItemReader<List<TratamientoLinea>>{

	private TratamientoLinea linea = null;
	
	@Autowired
	RepositoryItemReader<TratamientoLinea> itemReader;
	
	@PostConstruct
	public void posConstruct() throws UnexpectedInputException, ParseException, Exception {
		linea = this.itemReader.read();		
	}
	
	@Override
	public List<TratamientoLinea> read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if(this.linea==null)
			return null;
		List<TratamientoLinea> listaTratamientoLinea = new ArrayList<TratamientoLinea>();
		listaTratamientoLinea.add(this.linea);
		log.info("Guarda primero {}",linea);
		String clave = linea.getNombre();
		this.linea = this.itemReader.read();
		while(this.linea!=null && this.linea.getNombre().equals(clave)) {
			log.info("Guarda while {}",linea);
			listaTratamientoLinea.add(this.linea);
			this.linea = this.itemReader.read();
		}
		return listaTratamientoLinea;
	}
}
