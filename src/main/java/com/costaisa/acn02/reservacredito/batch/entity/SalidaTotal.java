package com.costaisa.acn02.reservacredito.batch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@lombok.Data
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@Entity
public class SalidaTotal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STID")
	Long id;
	
	@Column(name = "STNOMBRE")
	private String nombre;
	
	@Column(name = "STTOTAL")
	private int total;

}
