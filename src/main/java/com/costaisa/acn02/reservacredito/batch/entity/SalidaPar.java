package com.costaisa.acn02.reservacredito.batch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@lombok.Data
@lombok.AllArgsConstructor
@Entity
@Table(name = "salida_par")
public class SalidaPar {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SPID")
	Long id;
	
	@Column(name = "SPNOMBRE")
	private String nombre;
	
	@Column(name = "SPCANTIDAD")
	private int cantidad;
}
