package com.costaisa.acn02.reservacredito.batch.mapper;

import com.costaisa.acn02.reservacredito.batch.entity.SalidaImpar;
import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;
import com.costaisa.acn02.reservacredito.batch.model.LineaImpar;

public class LineaImparMapper {
	public static LineaImpar map(final TratamientoLinea tratamientoLinea) {
		return new LineaImpar(tratamientoLinea.getNombre(), tratamientoLinea.getCantidad());
	}
	
	public static SalidaImpar map(final LineaImpar lineaImpar) {
		return new SalidaImpar(null, lineaImpar.getNombre(), lineaImpar.getCantidad());
	}
}
