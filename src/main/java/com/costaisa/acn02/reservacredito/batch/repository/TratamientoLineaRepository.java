package com.costaisa.acn02.reservacredito.batch.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;

@Repository
public interface TratamientoLineaRepository extends PagingAndSortingRepository<TratamientoLinea, Long> {
	
	@Query(value = "SELECT u FROM TratamientoLinea u ORDER BY nombre")
	Page<TratamientoLinea> findSelect(Pageable pageable);

}
