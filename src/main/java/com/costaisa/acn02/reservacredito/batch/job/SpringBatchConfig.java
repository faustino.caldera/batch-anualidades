package com.costaisa.acn02.reservacredito.batch.job;


import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.builder.RepositoryItemReaderBuilder;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;
import com.costaisa.acn02.reservacredito.batch.model.DestinoLinea;
import com.costaisa.acn02.reservacredito.batch.repository.TratamientoLineaRepository;

@Configuration
@EnableBatchProcessing
public class SpringBatchConfig {

	@Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;

    @Bean
    public RepositoryItemReader<TratamientoLinea> itemReaderRepository(TratamientoLineaRepository repository) {
        final HashMap<String, Sort.Direction> sorts = new HashMap<>();
        return new RepositoryItemReaderBuilder<TratamientoLinea>()
        		.name("repositoryItemReader")
        		.pageSize(2)
        		.methodName("findSelect")
        		.sorts(sorts)
        		.repository(repository)
                .build();
    }

    @Bean
    @Qualifier("detalle")
    public JdbcBatchItemWriter<TratamientoLinea> itemWriterDetalle(DataSource dataSource) {
      return new JdbcBatchItemWriterBuilder<TratamientoLinea>()
        .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
        .sql("INSERT INTO salida (nombre, total) VALUES (:nombre, :total)")
        .dataSource(dataSource)
        .build();
    }

    @Bean
    @Qualifier("total")
    public JdbcBatchItemWriter<TratamientoLinea> itemWritertotal(DataSource dataSource) {
      return new JdbcBatchItemWriterBuilder<TratamientoLinea>()
        .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
        .sql("INSERT INTO salida_total (nombre, total) VALUES (:nombre, :total)")
        .dataSource(dataSource)
        .build();
    }
    
    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
      return this.jobs.get("importUserJob")
        .incrementer(new RunIdIncrementer())
        .listener(listener)
        .flow(step1)
        .end()
        .build();
    }

    @Bean
    public Step step1(ListLineaReader reader, ListLineaWriter writer, ListLineaProcessor processor) {
      return steps.get("step1")
        .<List<TratamientoLinea>, List<DestinoLinea>> chunk(10)
        .reader(reader)
        .processor(processor)
        .writer(writer)
        .build();
    }
}
