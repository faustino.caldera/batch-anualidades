package com.costaisa.acn02.reservacredito.batch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@lombok.Data
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class SalidaImpar {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SIID")
	Long id;
	
	@Column(name = "SINOMBRE")
	private String nombre;
	
	@Column(name = "SICANTIDAD")
	private int cantidad;
}
