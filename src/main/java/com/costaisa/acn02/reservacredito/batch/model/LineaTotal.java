package com.costaisa.acn02.reservacredito.batch.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@lombok.Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
public class LineaTotal extends LineaExtendida {
	private String nombre;
	private int cantidad;
}
