package com.costaisa.acn02.reservacredito.batch.mapper;

import com.costaisa.acn02.reservacredito.batch.entity.SalidaTotal;
import com.costaisa.acn02.reservacredito.batch.model.LineaTotal;

public class LineaTotalMapper {
	public static SalidaTotal map(LineaTotal lineaTotal) {
		return new SalidaTotal(null, lineaTotal.getNombre(), lineaTotal.getCantidad());
	}
}
