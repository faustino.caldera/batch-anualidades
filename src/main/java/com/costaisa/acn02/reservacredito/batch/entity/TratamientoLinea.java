package com.costaisa.acn02.reservacredito.batch.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@lombok.Data
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
@Entity
@Table(name = "tratalinea")
public class TratamientoLinea {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TLID")
	Long id;
	
	@Column(name = "TLNOMBRE")
	private String nombre;
	
	@Column(name = "TLCANTIDAD")
	private Integer cantidad;
}
