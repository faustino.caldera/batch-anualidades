package com.costaisa.acn02.reservacredito.batch.job;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;
import com.costaisa.acn02.reservacredito.batch.model.DestinoLinea;
import com.costaisa.acn02.reservacredito.batch.services.TratamientoLineaService;

import com.costaisa.acn02.reservacredito.batch.model.TipoTabla;

@Component
public class ListLineaWriter implements ItemWriter<List<DestinoLinea>> {
	@Autowired
	TratamientoLineaService tratamientoLineaService;

	@Override
	public void write(List<? extends List<DestinoLinea>> items) throws Exception {
		for(List<DestinoLinea> listDestinoLinea: items) {
			for(DestinoLinea destinoLinea: listDestinoLinea) {
				if(destinoLinea.getTipoTabla().equals(TipoTabla.IMPAR))
					this.tratamientoLineaService.writeImpar(destinoLinea.getLista());
				else if(destinoLinea.getTipoTabla().equals(TipoTabla.PAR))
					this.tratamientoLineaService.writePar(destinoLinea.getLista());
				else if(destinoLinea.getTipoTabla().equals(TipoTabla.TOTAL))
					this.tratamientoLineaService.writeTotal(destinoLinea.getLista());
			}
		}
	}

}
