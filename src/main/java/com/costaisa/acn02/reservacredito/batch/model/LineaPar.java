package com.costaisa.acn02.reservacredito.batch.model;

import lombok.EqualsAndHashCode;

@lombok.Data
@lombok.AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class LineaPar extends LineaExtendida {
	private String nombre;
	private int cantidad;
}
