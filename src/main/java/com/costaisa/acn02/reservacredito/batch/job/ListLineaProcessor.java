package com.costaisa.acn02.reservacredito.batch.job;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;
import com.costaisa.acn02.reservacredito.batch.mapper.LineaImparMapper;
import com.costaisa.acn02.reservacredito.batch.mapper.LineaParMapper;
import com.costaisa.acn02.reservacredito.batch.model.DestinoLinea;
import com.costaisa.acn02.reservacredito.batch.model.LineaExtendida;
import com.costaisa.acn02.reservacredito.batch.model.LineaTotal;
import com.costaisa.acn02.reservacredito.batch.model.TipoTabla;

@lombok.extern.slf4j.Slf4j
@Component
public class ListLineaProcessor implements ItemProcessor<List<TratamientoLinea>, List<DestinoLinea>> {
	
	@Override
	public List<DestinoLinea> process(final List<TratamientoLinea> lista) throws Exception {
		List<DestinoLinea> resultado = new ArrayList<DestinoLinea>();
		resultado.add(new DestinoLinea(TipoTabla.IMPAR, this.trataImpar(lista)));
		resultado.add(new DestinoLinea(TipoTabla.PAR, this.trataPar(lista)));
		resultado.add(new DestinoLinea(TipoTabla.TOTAL, this.trataTotal(lista)));
		log.info("Converting (" + lista + ") into (" + resultado + ")");
	    return resultado;
	}
	
	public List<LineaExtendida> trataImpar(List<TratamientoLinea> lista) {
		return lista.stream().filter(p -> p.getCantidad()%2 == 1).
				map(LineaImparMapper::map).collect(Collectors.toList());
	}

	public List<LineaExtendida> trataPar(List<TratamientoLinea> lista) {
		return lista.stream().filter(p -> p.getCantidad()%2 == 0).
				map(LineaParMapper::map).collect(Collectors.toList());
	}

	public List<LineaExtendida> trataTotal(List<TratamientoLinea> lista) {
		List<LineaExtendida> listLineaTotal = new ArrayList<LineaExtendida>();
		listLineaTotal.add(new LineaTotal(lista.get(0).getNombre(), lista.stream().mapToInt(p -> p.getCantidad()).sum()));
		return listLineaTotal;
	}
}
