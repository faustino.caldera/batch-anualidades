package com.costaisa.acn02.reservacredito.batch.model;

import java.util.List;

@lombok.Data
@lombok.AllArgsConstructor
@lombok.NoArgsConstructor
public class DestinoLinea {
	private TipoTabla tipoTabla;
	private List<LineaExtendida> lista;
}
