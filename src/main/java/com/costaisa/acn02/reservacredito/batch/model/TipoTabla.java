package com.costaisa.acn02.reservacredito.batch.model;

public enum TipoTabla {
	PAR,
	IMPAR,
	TOTAL
}
