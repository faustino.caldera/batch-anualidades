package com.costaisa.acn02.reservacredito.batch.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.costaisa.acn02.reservacredito.batch.entity.TratamientoLinea;
import com.costaisa.acn02.reservacredito.batch.mapper.LineaImparMapper;
import com.costaisa.acn02.reservacredito.batch.mapper.LineaParMapper;
import com.costaisa.acn02.reservacredito.batch.mapper.LineaTotalMapper;
import com.costaisa.acn02.reservacredito.batch.model.LineaExtendida;
import com.costaisa.acn02.reservacredito.batch.model.LineaImpar;
import com.costaisa.acn02.reservacredito.batch.model.LineaPar;
import com.costaisa.acn02.reservacredito.batch.model.LineaTotal;
import com.costaisa.acn02.reservacredito.batch.repository.SalidaImparRepository;
import com.costaisa.acn02.reservacredito.batch.repository.SalidaParRepository;
import com.costaisa.acn02.reservacredito.batch.repository.SalidaTotalRepository;

@Service
public class TratamientoLineaService {
	
	@Autowired
	private SalidaParRepository salidaParRepository;
	
	@Autowired
	private SalidaImparRepository salidaImparRepository;
	
	@Autowired
	private SalidaTotalRepository salidaTotalRepository;

	public void writeImpar(List<LineaExtendida> lista) {
		for(LineaExtendida linea: lista) {
			if(linea instanceof LineaImpar) {
				this.salidaImparRepository.save(LineaImparMapper.map((LineaImpar)linea));
			}
		}
	}
	
	public void writePar(List<LineaExtendida> lista) {
		for(LineaExtendida linea: lista) {
			if(linea instanceof LineaPar) {
				this.salidaParRepository.save(LineaParMapper.map((LineaPar)linea));
			}
		}
	}
	public void writeTotal(List<LineaExtendida> lista) {
		for(LineaExtendida linea: lista) {
			if(linea instanceof LineaTotal) {
				this.salidaTotalRepository.save(LineaTotalMapper.map((LineaTotal)linea));
			}
		}
	}
}
