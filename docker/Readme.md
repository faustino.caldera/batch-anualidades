Levantar el docker con:
	
	docker-compose up --build

Docker ejecutandose:

	docker ps # igual que docker container ls
	docker ps -a # visualiza incluso los parados
	
Para los docker que se esta ejecutando

	docker stop $(docker ps -q)
	
Para borrar contenedores:

	docker rm
	docker rm $(docker ps -aq) # borrar todos los contenedores. Deben de estar parados
	
Borrar imagenes:

	docker images
	docker rmi <nombre>
	docker rmi $(docker images -a -q)

	